package com.nickmcdev.dynaexample.fragments;

/**
 * @author nicholas.mcwherter
 */

import static com.nickmcdev.dynaexample.utility.Constants.ERROR_WR;
import static com.nickmcdev.dynaexample.utility.Constants.JSON_WR;
import static com.nickmcdev.dynaexample.utility.Constants.TAG_WR;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.fragment.app.Fragment;

// okhttp3:
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

import com.nickmcdev.dynaexample.R;
import com.nickmcdev.dynaexample.utility.WebRequests;

//import com.dynatrace.android.agent.Dynatrace;

public class WebRequestFragment extends Fragment implements View.OnClickListener {

	TextView cardText;
	EditText wrEditText;
	Button wrButton;

	String department = "Unknown";
	String title = "Unknown";
	String fName = "John";
	String lName = "Deer";
	String email = "unknown@unknown.com";
	JSONObject json = new JSONObject();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.webrequest_layout, container, false);

		cardText = rootView.findViewById(R.id.card_text);
		wrEditText = rootView.findViewById(R.id.wr_edit_text);
		wrButton = rootView.findViewById(R.id.wr_button);

		wrButton.setOnClickListener(this);


	return rootView;

	}


	@Override
	public void onClick(View view) {
		WebRequests testRequest = new WebRequests();
		switch (view.getId()) {
		case R.id.wr_edit_text:
			Log.i("DynaWR", "Make Request button clicked!");
			//asyncGet(url);
			break;
		case R.id.wr_button:
			Log.i("DynaWR", "Make Request button clicked!");
			//asyncGet(url);
			makeRequest();
			break;
		}
	}

	void asyncGet(String url) {

		OkHttpClient client = new OkHttpClient();

		Request request = new Request.Builder()
				.url(url)
				.build();

		client.newCall(request).enqueue(new Callback() {
			@Override
			public void onFailure(Call call, IOException e) {
				call.cancel();
				Log.i("DynaWR", "Async GET failed with the following exception: " + e.toString());


				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						cardText.setText("The request failed with an exception.");
					}
				});
			}
			@Override
			public void onResponse(Call call, Response response) throws IOException {
				final String resp = response.body().string();

				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						Log.i("DynaWR", "Successful async GET response: " + resp);
						try {
							json = new JSONObject(resp);
							department = json.getJSONObject("data").getString("department");
							title = json.getJSONObject("data").getString("title");
							fName = json.getJSONObject("data").getString("firstname");
							lName = json.getJSONObject("data").getString("lastname");
							email = json.getJSONObject("data").getString("email");
						} catch (JSONException e) {
							e.printStackTrace();
						}
						cardText.setText(department + "\n\n" + title + "\n\n" + fName + " " + lName + "\n\n" + email);
					}
				});
			}
		});
	}

	void makeRequest() {
		switch (wrEditText.getText().toString()) {
		case "1":
			asyncGet(JSON_WR[0]);
			Log.i(TAG_WR, "Make Request button clicked! URL of request: " + JSON_WR[0]);
			break;
		case "2":
			asyncGet(JSON_WR[1]);
			Log.i(TAG_WR, "Make Request button clicked! URL of request: " + JSON_WR[1]);
			break;
		case "3":
			asyncGet(JSON_WR[2]);
			Log.i(TAG_WR, "Make Request button clicked! URL of request: " + JSON_WR[2]);
			break;
		case "4":
			asyncGet(JSON_WR[3]);
			Log.i(TAG_WR, "Make Request button clicked! URL of request: " + JSON_WR[3]);
			break;
		case "5":
			asyncGet(JSON_WR[4]);
			Log.i(TAG_WR, "Make Request button clicked! URL of request: " + JSON_WR[4]);
			break;
		case "6":
			asyncGet(JSON_WR[5]);
			Log.i(TAG_WR, "Make Request button clicked! URL of request: " + JSON_WR[5]);
			break;
		case "7":
			asyncGet(JSON_WR[6]);
			Log.i(TAG_WR, "Make Request button clicked! URL of request: " + JSON_WR[6]);
			break;
		case "8":
			asyncGet(JSON_WR[7]);
			Log.i(TAG_WR, "Make Request button clicked! URL of request: " + JSON_WR[7]);
			break;
		case "9":
			asyncGet(JSON_WR[8]);
			Log.i(TAG_WR, "Make Request button clicked! URL of request: " + JSON_WR[8]);
			break;
		case "10":
			asyncGet(JSON_WR[9]);
			Log.i(TAG_WR, "Make Request button clicked! URL of request: " + JSON_WR[9]);
			break;
		case "11":
			asyncGet(JSON_WR[10]);
			Log.i(TAG_WR, "Make Request button clicked! URL of request: " + JSON_WR[10]);
			break;
		case "12":
			asyncGet(JSON_WR[11]);
			Log.i(TAG_WR, "Make Request button clicked! URL of request: " + JSON_WR[11]);
			break;
		case "13":
			asyncGet(JSON_WR[12]);
			Log.i(TAG_WR, "Make Request button clicked! URL of request: " + JSON_WR[12]);
			break;
		case "14":
			asyncGet(JSON_WR[13]);
			Log.i(TAG_WR, "Make Request button clicked! URL of request: " + JSON_WR[13]);
			break;
		case "15":
			asyncGet(JSON_WR[14]);
			Log.i(TAG_WR, "Make Request button clicked! URL of request: " + JSON_WR[14]);
			break;
		case "16":
			asyncGet(JSON_WR[15]);
			Log.i(TAG_WR, "Make Request button clicked! URL of request: " + JSON_WR[15]);
			break;
		case "17":
			asyncGet(JSON_WR[16]);
			Log.i(TAG_WR, "Make Request button clicked! URL of request: " + JSON_WR[16]);
			break;
		case "18":
			asyncGet(JSON_WR[17]);
			Log.i(TAG_WR, "Make Request button clicked! URL of request: " + JSON_WR[17]);
			break;
		case "19":
			asyncGet(JSON_WR[18]);
			Log.i(TAG_WR, "Make Request button clicked! URL of request: " + JSON_WR[18]);
			break;
		case "20":
			asyncGet(JSON_WR[19]);
			Log.i(TAG_WR, "Make Request button clicked! URL of request: " + JSON_WR[19]);
			break;
		case "21":
			asyncGet(JSON_WR[20]);
			Log.i(TAG_WR, "Make Request button clicked! URL of request: " + JSON_WR[20]);
			break;
		case "22":
			asyncGet(JSON_WR[21]);
			Log.i(TAG_WR, "Make Request button clicked! URL of request: " + JSON_WR[21]);
			break;
		case "23":
			asyncGet(JSON_WR[22]);
			Log.i(TAG_WR, "Make Request button clicked! URL of request: " + JSON_WR[22]);
			break;
		case "24":
			asyncGet(JSON_WR[23]);
			Log.i(TAG_WR, "Make Request button clicked! URL of request: " + JSON_WR[23]);
			break;
		case "25":
			asyncGet(JSON_WR[24]);
			Log.i(TAG_WR, "Make Request button clicked! URL of request: " + JSON_WR[24]);
			break;
		case "26":
			asyncGet(JSON_WR[25]);
			Log.i(TAG_WR, "Make Request button clicked! URL of request: " + JSON_WR[25]);
			break;
		case "27":
			asyncGet(JSON_WR[26]);
			Log.i(TAG_WR, "Make Request button clicked! URL of request: " + JSON_WR[26]);
			break;
		case "28":
			asyncGet(JSON_WR[27]);
			Log.i(TAG_WR, "Make Request button clicked! URL of request: " + JSON_WR[27]);
			break;
		case "29":
			asyncGet(JSON_WR[28]);
			Log.i(TAG_WR, "Make Request button clicked! URL of request: " + JSON_WR[28]);
			break;
		case "30":
			asyncGet(JSON_WR[29]);
			Log.i(TAG_WR, "Make Request button clicked! URL of request: " + JSON_WR[29]);
			break;
		case "31":
			asyncGet(JSON_WR[30]);
			Log.i(TAG_WR, "Make Request button clicked! URL of request: " + JSON_WR[30]);
			break;
		case "32":
			asyncGet(ERROR_WR[0]);
			Log.i(TAG_WR, "Make Request button clicked! URL of request: " + ERROR_WR[0]);
			break;
		case "33":
			asyncGet(ERROR_WR[1]);
			Log.i(TAG_WR, "Make Request button clicked! URL of request: " + ERROR_WR[1]);
			break;
		case "34":
			asyncGet(ERROR_WR[2]);
			Log.i(TAG_WR, "Make Request button clicked! URL of request: " + ERROR_WR[2]);
			break;
		case "35":
			asyncGet(ERROR_WR[3]);
			Log.i(TAG_WR, "Make Request button clicked! URL of request: " + ERROR_WR[3]);
			break;
		}

	}


}
