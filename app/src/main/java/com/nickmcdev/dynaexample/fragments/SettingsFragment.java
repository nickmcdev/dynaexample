package com.nickmcdev.dynaexample.fragments;

import static com.nickmcdev.dynaexample.utility.Constants.LOCATIONS;
import static com.nickmcdev.dynaexample.utility.Constants.TAG_AUTH;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.fragment.app.Fragment;
import jp.wasabeef.picasso.transformations.CropCircleTransformation;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.nickmcdev.dynaexample.R;
import com.nickmcdev.dynaexample.activities.LoginActivity;
import com.squareup.picasso.Picasso;

/**
 * @author nicholas.mcwherter
 */
public class SettingsFragment extends Fragment implements View.OnClickListener {

	// Shared Prefs
	SharedPreferences sharedPref;
	SharedPreferences.Editor editor;

	public GoogleSignInClient gSignInClient;
	private FirebaseAuth mAuth;
	private TextView name, email, location;
	private Button editLocation, gSignOut, boom;
	private ImageView gProfileImage;

	String gName = "Test";
	String gEmail = "test@gmail.com";
	String gProfileImageUrl = "https://test.google.com";



	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.settings_layout, container, false);

		// Initialize Shared Prefs
		sharedPref = getContext().getSharedPreferences("DynaExampleConfig", 0);
		editor = sharedPref.edit();

		mAuth = FirebaseAuth.getInstance();

		GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
				.requestIdToken(getString(R.string.default_web_client_id))
				.requestId()
				.requestProfile()
				.requestEmail()
				.build();

		gSignInClient = GoogleSignIn.getClient(getActivity(), gso);

		GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(getActivity());
		if (account != null) {
			gName = account.getDisplayName();
			gEmail = account.getEmail();
			gProfileImageUrl = account.getPhotoUrl().toString();
		}

		name = rootView.findViewById(R.id.name_text);
		email = rootView.findViewById(R.id.user_email);
		location = rootView.findViewById(R.id.location_text);
		gProfileImage = rootView.findViewById(R.id.profile_image);
		editLocation = rootView.findViewById(R.id.edit_location);
		gSignOut = rootView.findViewById(R.id.gsign_out);

		Bitmap bitmap = BitmapFactory.decodeResource(getResources(),R.id.profile_image);

		RoundedBitmapDrawable round = RoundedBitmapDrawableFactory.create(getResources(), bitmap);
		round.setCircular(true);
		gProfileImage.setImageDrawable(round);

		editLocation.setOnClickListener(this);
		gSignOut.setOnClickListener(this);

		loadData();

		return rootView;
	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.edit_location:
			selectLocation();
			break;
		case R.id.gsign_out:
			signOutOfFirebaseAndGoogle();
			break;
		}
	}

	private void loadData() {
		Picasso.get()
				.load(gProfileImageUrl)
				.resize(200,200)
				.transform(new CropCircleTransformation())
				.into(gProfileImage);
		Log.d(TAG_AUTH, "Google Account Profile Image URL: " + gProfileImageUrl);
	 	name.setText(gName);
		Log.d(TAG_AUTH, "Google Account Name: " + gName);
	 	email.setText(gEmail);
		Log.d(TAG_AUTH, "Google Account Email: " + gEmail);
		location.setText(sharedPref.getString("location", "No location selected!"));
		Log.d(TAG_AUTH, "Location: " + sharedPref.getString("location", "No location selected!"));
	}

	private void signOutOfFirebaseAndGoogle() {
		mAuth.signOut();
		gSignInClient.signOut()
				.addOnCompleteListener(getActivity(), new OnCompleteListener<Void>() {
			@Override
			public void onComplete(@NonNull Task<Void> task) {
				Log.d(TAG_AUTH, "User logged out of Firebase and Google Sign in! Routing to LoginActivity.");

				// Sign out and segue/route to Login Activity
				Intent intent = new Intent(getActivity(), LoginActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
			}
		});
	}

	private void selectLocation() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
		builder.setTitle("Select your new environment:");
		builder.setCancelable(false);

		builder.setItems(LOCATIONS, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
				case 0:
					// Clear old location and store new location in SharedPref to DEV
					editor.remove("location");
					Log.d(TAG_AUTH, LOCATIONS[0] + " selected!");
					editor.putString("location", LOCATIONS[0]);
					editor.commit();

					location.setText(sharedPref.getString("location", "No environment selected!"));
					Log.d(TAG_AUTH, "Environment: " + sharedPref.getString("location", "No environment selected!"));

					dialog.dismiss();
					break;
				case 1:
					// Clear old location and store new location in SharedPref to SPRINT
					editor.remove("location");
					Log.d(TAG_AUTH, LOCATIONS[1] + " selected!");
					editor.putString("location", LOCATIONS[1]);
					editor.commit();

					location.setText(sharedPref.getString("location", "No environment selected!"));
					Log.d(TAG_AUTH, "Environment: " + sharedPref.getString("location", "No environment selected!"));

					dialog.dismiss();
					break;
				case 2:
					// Clear old location and store new location in SharedPref to PROD
					editor.remove("location");
					Log.d(TAG_AUTH, LOCATIONS[2] + " selected!");
					editor.putString("location", LOCATIONS[2]);
					editor.commit();

					location.setText(sharedPref.getString("location", "No environment selected!"));
					Log.d(TAG_AUTH, "Environment: " + sharedPref.getString("location", "No environment selected!"));

					dialog.dismiss();
					break;
				}
			}
		});

		// create and show the alert dialog
		AlertDialog dialog = builder.create();
		dialog.show();
	}
}
