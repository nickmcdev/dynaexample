package com.nickmcdev.dynaexample.fragments;

/**
 * @author nicholas.mcwherter
 */
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import androidx.fragment.app.Fragment;

import com.nickmcdev.dynaexample.R;

public class UserActionFragment extends Fragment implements View.OnClickListener {

	Button singleActionButton, subActionButton;
	TextView cardText;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.useraction_layout, container, false);

		cardText = rootView.findViewById(R.id.card_text);
		singleActionButton = rootView.findViewById(R.id.singleActionButton);
		subActionButton = rootView.findViewById(R.id.subActionButton);

		singleActionButton.setOnClickListener(this);
		subActionButton.setOnClickListener(this);



		return rootView;
	}
	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.singleActionButton:
			Log.i("DynaAction", "Single Action Button Clicked!");
			cardText.setText("Single Action Button Clicked!");
			break;
		case R.id.subActionButton:
			Log.i("DynaAction", "Sub Action Button Clicked!");
			cardText.setText("Sub Action Button Clicked!");
			delaySecond("First sub action!");
			delayFiveSeconds("Second sub action!");

			break;
		}
	}
	void delaySecond(final String cardTextString) {
		final long startTime = System.currentTimeMillis();
		Log.i("DynaAction", "First action delay started!");
		Log.i("DynaAction", "Current time in ms: " + startTime);
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				long endTimeFirstSub = System.currentTimeMillis();
				Log.i("DynaAction", "Current time in ms: " + endTimeFirstSub);
				long responseTimeL = (endTimeFirstSub - startTime);
				String responseTime = String.valueOf(responseTimeL);
				cardText.setText(cardTextString + "\nResponse time: " + responseTime + "ms");
				Log.i("DynaAction", "First action delay ended!");
			}
		}, 1000);
	}

	void delayFiveSeconds(final String cardTextString) {
		final long startTime = System.currentTimeMillis();
		Log.i("DynaAction", "Second action delay started!");
		Log.i("DynaAction", "Current time in ms: " + startTime);
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				long endTimeSecondSub = System.currentTimeMillis();
				Log.i("DynaAction", "Current time in ms: " + endTimeSecondSub);
				long responseTimeL = (endTimeSecondSub - startTime);
				String responseTime = String.valueOf(responseTimeL);
				cardText.setText(cardTextString + "\n\nResponse time: " + responseTime + "ms");
				Log.i("DynaAction", "Second action delay ended!");
			}
		}, 5000);
	}


	@Override
	public void onPause()
	{
		super.onPause();
	}

	@Override
	public void onStart()
	{
		super.onStart();
	}

	@Override
	public void onResume()
	{
		super.onResume();
	}

	@Override
	public void onDestroy()
	{
		super.onDestroy();
	}

	@Override
	public void onLowMemory()
	{
		super.onLowMemory();
	}
}
