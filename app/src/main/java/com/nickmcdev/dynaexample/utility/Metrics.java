package com.nickmcdev.dynaexample.utility;

import static com.nickmcdev.dynaexample.utility.Constants.TAG_METRICS;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.BatteryManager;
import android.telephony.TelephonyManager;
import android.util.Log;

/**
 * @author nicholas.mcwherter
 */
public class Metrics {

	/**
	 Logic in gathering Network/Mobile connection type
	 -------------------------------------------------
	 */
	public static String getMobileConnType(Context c) {
		TelephonyManager tm = (TelephonyManager)
				c.getSystemService(Context.TELEPHONY_SERVICE);
		int networkType = tm.getNetworkType();
		switch (networkType) {
			// 2G network types:
		case TelephonyManager.NETWORK_TYPE_GPRS:
		case TelephonyManager.NETWORK_TYPE_EDGE:
		case TelephonyManager.NETWORK_TYPE_CDMA:
		case TelephonyManager.NETWORK_TYPE_1xRTT:
		case TelephonyManager.NETWORK_TYPE_IDEN:
			Log.d(TAG_METRICS, "Connection type found: 2G");
			return "2G";

			// 3G network types:
		case TelephonyManager.NETWORK_TYPE_UMTS:
		case TelephonyManager.NETWORK_TYPE_EVDO_0:
		case TelephonyManager.NETWORK_TYPE_EVDO_A:
		case TelephonyManager.NETWORK_TYPE_HSDPA:
		case TelephonyManager.NETWORK_TYPE_HSUPA:
		case TelephonyManager.NETWORK_TYPE_HSPA:
		case TelephonyManager.NETWORK_TYPE_EVDO_B:
		case TelephonyManager.NETWORK_TYPE_EHRPD:
		case TelephonyManager.NETWORK_TYPE_HSPAP:
			Log.d(TAG_METRICS, "Connection type found: 3G");
			return "3G";

			// 4G
		case TelephonyManager.NETWORK_TYPE_LTE:
		case TelephonyManager.NETWORK_TYPE_IWLAN:
			Log.d(TAG_METRICS, "Connection type found: LTE/4G");
			return "LTE/4G";

//		case TelephonyManager.NETWORK_TYPE_NR:
//			Log.d(TAG_METRICS, "Connection type found: 5G");
//			return "5G";

		default:
			return "Unknown!";
		}
	}

	// To check if the device has a network connection and the type of network connection (WIFI or Mobile)
	public String getNetworkStatusAndType(Context c) {
		String connType = "Unknown";

		ConnectivityManager cm = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo nI = cm.getActiveNetworkInfo();
		if (nI != null && nI.isConnected()) {
			if (nI.getType() == ConnectivityManager.TYPE_WIFI) {
				connType = "Connection type: Wifi";
				Log.d(TAG_METRICS, "Connection type found: " + connType);
			} else if (nI.getType() == ConnectivityManager.TYPE_MOBILE) {
				connType = getMobileConnType(c);
				Log.d(TAG_METRICS, "Connection type found: " + connType);
			}
		} else {
			Log.d(TAG_METRICS, "Device is not connected or connection type is " + connType);
			return connType;
		}
		return "Device is not connected or connection type is unknown!";
	}

	/**
	 Battery level logic:
	 --------------------
	 */

	public int getBatteryLevel(Context c) {
		int batteryPercentage = -1;
		BatteryManager bm = (BatteryManager) c.getSystemService(Context.BATTERY_SERVICE);
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
			batteryPercentage = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
			Log.i(TAG_METRICS, "Battery percentage is: " + batteryPercentage + "%");
		}
		return batteryPercentage;
	}


}
