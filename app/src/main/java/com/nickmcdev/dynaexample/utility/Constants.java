package com.nickmcdev.dynaexample.utility;

/**
 * @author nicholas.mcwherter
 */
public class Constants {

	// Logging
	public static final String TAG_AUTH = "DynaAuthInfo";
	public static final String TAG_METRICS = "DynaMetricInfo";
	public static final String TAG_WR = "DynaWebRequestInfo";

	// Web request
	public static final int RC_SIGN_IN = 9001;
	public static final String[] LOCATIONS = {"DEV", "SPRINT", "PROD"};
	public static final String JSON_URL = "http://nickmcapache1.dtwlab.dynatrace.org:81/json/services/";
	public static final String[] JSON_WR = {JSON_URL + "aleksey.json", JSON_URL + "anthony.json", JSON_URL + "ari.json", JSON_URL + "axel.json", JSON_URL + "bart.json", JSON_URL + "brett.json", JSON_URL + "daniele.json", JSON_URL + "dave.json", JSON_URL + "derek.json", JSON_URL + "dong.json", JSON_URL + "eldrige.json", JSON_URL + "eric.json", JSON_URL + "gagandeep.json", JSON_URL + "james.json", JSON_URL + "joe.json", JSON_URL + "john.json", JSON_URL + "johnr.json", JSON_URL + "josh.json", JSON_URL + "jp.json", JSON_URL + "kenneth.json", JSON_URL + "kia.json", JSON_URL + "kyle.json", JSON_URL + "markie.json", JSON_URL + "matthew.json", JSON_URL + "moe.json", JSON_URL + "nick.json", JSON_URL + "rodrigo.json", JSON_URL + "rodrigoa.json", JSON_URL + "shane.json", JSON_URL + "shaun.json", JSON_URL + "trevor.json"};
	public static final String[] ERROR_WR = {JSON_URL + "404.json", "http://nickmcapache1.dtwlab.dynatrace.net:81/json/services/nick.json", "http://nickmcapache1.dtwlab.dynatrace.com:81/json/services/nick.json", "http://nickmcapache1.dtwlab.dynatrace.org/json/services/nick.json"};

	// Dynatrace info
	public static final String[] BREAKOUT_DEV = {"6c4f3de9-e2d0-4bd6-9bf5-307cc0c0c3de", "https://bf34473abw.bf-dev.dynatracelabs.com/mbeacon"};
	public static final String[] BREAKOUT_SPRINT = {"5c8f7af6-377c-4483-bda9-c3671cbe9cd6", "http://nickmcapache1.dtwlab.dynatrace.org:81/mbeacon"};
	public static final String[] BREAKOUT_PROD = {"cd4908b4-0ce6-4247-8592-3d2d60ff87e0", "https://bf96722syz.bf.dynatrace.com/mbeacon"};

}
