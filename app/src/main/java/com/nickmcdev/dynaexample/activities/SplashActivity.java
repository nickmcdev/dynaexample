package com.nickmcdev.dynaexample.activities;

/**
 * @author nicholas.mcwherter
 */
import static com.nickmcdev.dynaexample.utility.Constants.*;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.nickmcdev.dynaexample.R;
import com.nickmcdev.dynaexample.utility.Metrics;

public class SplashActivity extends AppCompatActivity {

	private FirebaseAuth mAuth;
	Metrics metrics = new Metrics();


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash_layout);
		mAuth = FirebaseAuth.getInstance();
		final FirebaseUser user = mAuth.getCurrentUser();

		Thread timerThread = new Thread() {
			public void run() {
				try {
					sleep(3000);
				}
				catch (InterruptedException e) {
					e.printStackTrace();
				} finally {
					checkIfUserLoggedIn(user);
					metrics.getBatteryLevel(SplashActivity.this);
					metrics.getNetworkStatusAndType(SplashActivity.this);

				}
			}
		};
		timerThread.start();
	}

	private void checkIfUserLoggedIn(FirebaseUser user) {
		if (user != null) {
			Log.d(TAG_AUTH, "User is currently logged in! Routing to TabActivity.");
			// TODO: Validate that LOCATION is set in SharedPrefs
			// Route to Main view
			Intent intent = new Intent(SplashActivity.this, TabActivity.class);
			startActivity(intent);
		} else {
			Log.d(TAG_AUTH, "No user logged in. Routing to LoginActivity.");
			Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
			startActivity(intent);
		}
	}

}