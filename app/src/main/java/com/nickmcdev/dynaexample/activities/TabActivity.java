package com.nickmcdev.dynaexample.activities;

import android.os.Bundle;
import android.view.MenuItem;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.nickmcdev.dynaexample.R;
import com.nickmcdev.dynaexample.fragments.SettingsFragment;
import com.nickmcdev.dynaexample.fragments.UserActionFragment;
import com.nickmcdev.dynaexample.fragments.WebRequestFragment;

/**
 * @author nicholas.mcwherter
 */
public class TabActivity extends AppCompatActivity {

	final Fragment userActionFragment = new UserActionFragment();
	final Fragment webRequestFragment = new WebRequestFragment();
	final Fragment settingsFragment = new SettingsFragment();
	final FragmentManager fm = getSupportFragmentManager();

	Fragment activeFrag = userActionFragment;

	private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
			= new BottomNavigationView.OnNavigationItemSelectedListener() {

		@Override
		public boolean onNavigationItemSelected(@NonNull MenuItem item) {
			switch (item.getItemId()) {
			case R.id.nav_ua:
				fm.beginTransaction().hide(activeFrag).show(userActionFragment).commit();
				activeFrag = userActionFragment;
				return true;
			case R.id.nav_wr:
				fm.beginTransaction().hide(activeFrag).show(webRequestFragment).commit();
				activeFrag = webRequestFragment;
				return true;
			case R.id.nav_settings:
				fm.beginTransaction().hide(activeFrag).show(settingsFragment).commit();
				activeFrag = settingsFragment;
				return true;
			}
			return false;
		}
	};


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tab_activity);

		fm.beginTransaction().add(R.id.main_container, userActionFragment, "1").commit();
		fm.beginTransaction().add(R.id.main_container, webRequestFragment, "2").hide(webRequestFragment).commit();
		fm.beginTransaction().add(R.id.main_container, settingsFragment, "3").hide(settingsFragment).commit();

		BottomNavigationView navView = findViewById(R.id.navigation);

		navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


	}

	@Override
	public void onBackPressed() { }
}
