package com.nickmcdev.dynaexample.activities;

import static com.nickmcdev.dynaexample.utility.Constants.LOCATIONS;
import static com.nickmcdev.dynaexample.utility.Constants.RC_SIGN_IN;
import static com.nickmcdev.dynaexample.utility.Constants.TAG_AUTH;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.nickmcdev.dynaexample.R;

public class LoginActivity extends AppCompatActivity {

	// SharedPrefs
	private String locationSelected = "";
	private String gEmail = "";

	// Initialize SharedPref
	SharedPreferences sharedPref;
	SharedPreferences.Editor editor;

	// Google/Firebase Sign in
	public GoogleSignInClient gSignInClient;
	private FirebaseAuth mAuth;
	private FirebaseAuth.AuthStateListener mAuthListener;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_layout);

		// Initialize SharedPrefs
		sharedPref = LoginActivity.this.getSharedPreferences("DynaExampleConfig", 0);
		editor = sharedPref.edit();

		// Check to see if a location was selected.
		isLocationSelected();


		// Google Sign-in stuff
		SignInButton gSignInButton = findViewById(R.id.gsignin_button);
		gSignInButton.setColorScheme(SignInButton.COLOR_DARK);
		gSignInButton.setSize(SignInButton.SIZE_WIDE);

		GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
				.requestIdToken(getString(R.string.default_web_client_id))
				.requestId()
				.requestProfile()
				.requestEmail()
				.build();

		gSignInClient = GoogleSignIn.getClient(this, gso);

		mAuth = FirebaseAuth.getInstance();



		gSignInButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				gSignIn();
			}
		});
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == RC_SIGN_IN) {
			Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
			try {
				GoogleSignInAccount account = task.getResult(ApiException.class);
				firebaseAuthWithGoogle(account);
				Log.d(TAG_AUTH, "Login successful!");
			} catch (ApiException e) {
				Log.w(TAG_AUTH, "Google sign in failed: e" + e);
			}
		}
	}

	private void firebaseAuthWithGoogle(GoogleSignInAccount account) {
		Log.d(TAG_AUTH, "GoogleID: " + account.getId());
		AuthCredential creds = GoogleAuthProvider.getCredential(account.getIdToken(), null);
		mAuth.signInWithCredential(creds)
				.addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
					@Override
					public void onComplete(@NonNull Task<AuthResult> task) {
						if (task.isSuccessful()) {
							// Sign in success, update UI with the signed-in user's information
							Log.d(TAG_AUTH, "signInWithCredential:success!");
							FirebaseUser user = mAuth.getCurrentUser();
							afterAuthAttempt(user);

							gEmail = user.getEmail();
							// Store selected location in SharedPref
							editor.putString("gEmail", gEmail);
							editor.commit();

						} else {
							Log.w(TAG_AUTH, "signInWithCredential:failure: ", task.getException());
						}
					}
				});
	}

	private void afterAuthAttempt(FirebaseUser user) {
		if (user != null) {
			// Route to Main view
			Intent intent = new Intent(LoginActivity.this, TabActivity.class);
			startActivity(intent);
		} else {
			Toast.makeText(LoginActivity.this, "Login failed. Please try again!", Toast.LENGTH_SHORT).show();
		}
	}

	private void gSignIn() {
		Intent signInIntent = gSignInClient.getSignInIntent();
		startActivityForResult(signInIntent, RC_SIGN_IN);
	}

	private void selectLocation() {
		AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
		builder.setTitle("Select your location:");

		builder.setItems(LOCATIONS, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
				case 0:
					// set location in SharedPref to DEV
					Log.d("DynaLocation", LOCATIONS[0] + " selected!");
					locationSelected = LOCATIONS[0];

					// Store selected location in SharedPref
					editor.putString("location", locationSelected);
					editor.commit();

					dialog.dismiss();
					break;
				case 1:
					// set location in SharedPref to SPRINT
					Log.d("DynaLocation", LOCATIONS[1] + " selected!");
					locationSelected = LOCATIONS[1];

					// Store selected location in SharedPref
					editor.putString("location", locationSelected);
					editor.commit();

					dialog.dismiss();
					break;
				case 2:
					// Set location in SharedPref to PROD
					Log.d("DynaLocation", LOCATIONS[2] + " selected!");
					locationSelected = LOCATIONS[2];

					// Store selected location in SharedPref
					editor.putString("location", locationSelected);
					editor.commit();

					dialog.dismiss();
					break;
				}
			}
		});

		// Create and show the alert dialog
		AlertDialog dialog = builder.create();
		dialog.show();
	}

	private void validateLocation() {
		AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
		builder
				.setTitle("Did you still want to use " + sharedPref.getString("location", null) + " for your environment?")
				.setCancelable(false)
				.setNegativeButton("Pick new environment",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {

						// Remove location from SharedPref and save changes
						editor.remove("location");
						editor.commit();

						dialog.dismiss();
						selectLocation();
					}
				})
				.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						dialog.dismiss();
					}
				});

				AlertDialog dialog = builder.create();
				dialog.show();
	}

	private void isLocationSelected() {

		if (sharedPref.contains("location") == true && gEmail == "") {
			validateLocation();
		} else if (sharedPref.contains("location") == false) {
			selectLocation();
		}
	}
}
